package com.verhas.jira.plugin.customfields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.impl.AbstractMultiCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.ErrorCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class MergeCustomField extends AbstractMultiCFType<String> {

    private static final Logger log = LoggerFactory.getLogger(MergeCustomField.class);
    private final IssueManager issueManager;

    public MergeCustomField(CustomFieldValuePersister customFieldValuePersister,
                            GenericConfigManager genericConfigManager,
                            IssueManager issueManager) {
        super(customFieldValuePersister, genericConfigManager);
        this.issueManager = issueManager;
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        //add what you need to the map here

        return map;
    }

    @Override
    protected Comparator<String> getTypeComparator() {
        return null;
    }

    @Override
    protected Object convertTypeToDbValue(String value) {
        return getStringFromSingularObject(value);
    }

    @Override
    protected String convertDbValueToType(Object dbValue) {
        return getSingularObjectFromString((String) dbValue);
    }

    @Override
    protected PersistenceFieldType getDatabaseType() {
        return PersistenceFieldType.TYPE_LIMITED_TEXT;
    }

    @Override
    public String getStringFromSingularObject(String s) {
        return s;
    }

    @Override
    public String getSingularObjectFromString(String s) throws FieldValidationException {
        return s;
    }

    @Override
    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config) {
        final Collection<String> param = relevantParams.getValuesForNullKey();
        final CustomField customField = config.getCustomField();

        if (param.size()!=1) {
            errorCollectionToAddTo.addError(customField.getId(), getI18nBean().getText("admin.errors.specify.one"), ErrorCollection.Reason.VALIDATION_FAILED);
        } else {
            // Validate issue key
            if (null == issueManager.getIssueObject(param.iterator().next())) {
                errorCollectionToAddTo.addError(customField.getId(),
                        getI18nBean().getText("admin.errors.invalid.value.passed.for.issue",
                                "'" + param + "'", "'" + customField + "'"), ErrorCollection.Reason.VALIDATION_FAILED);
            }
        }
    }

    @Override
    public Collection<String> getValueFromCustomFieldParams(CustomFieldParams parameters) throws FieldValidationException {
        final Collection<?> values = parameters.getAllValues();
        if (CustomFieldUtils.isCollectionNotEmpty(values)) {
            List<String> issueKeys = new ArrayList<String>();
            for (Object value : values) {
                issueKeys.add(getSingularObjectFromString((String) value));
            }
            return issueKeys;
        } else {
            return null;
        }

    }

    @Override
    public Object getStringValueFromCustomFieldParams(CustomFieldParams parameters) {
        return parameters.getValuesForNullKey();
    }
}