package com.verhas.jira.plugin.listeners;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.fields.screen.FieldScreenService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenFactory;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.verhas.jira.plugin.merger.MergeIssuesFunction;
import org.ofbiz.core.entity.GenericEntityException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PluginEnabledListener implements InitializingBean, DisposableBean {

    private static final List<IssueType> allIssues=new ArrayList<>();
    private static final List<JiraContextNode> allProjects = new ArrayList<>();
    private static final String MERGE_SCREEN = "Merge screen";

    {
        allIssues.add(GlobalIssueContext.getInstance().getIssueType()) ;
        allProjects.add(GlobalIssueContext.getInstance());
    }
    private static final CustomFieldSearcher NO_CUSTOMFIELD_SEARCHER = null;
    public static final String PLUGIN_KEY = "com.verhas.jira.plugin.jira-issue-merger";
    private final EventPublisher eventPublisher;
    private final CustomFieldManager customFieldManager;
    private final FieldScreenManager fieldScreenManager;
    private final FieldScreenFactory fieldScreenFactory;
    public PluginEnabledListener(EventPublisher eventPublisher, CustomFieldManager customFieldManager, FieldScreenManager fieldScreenManager, FieldScreenFactory fieldScreenFactory) {
        this.eventPublisher=eventPublisher;
        this.customFieldManager = customFieldManager;
        this.fieldScreenManager = fieldScreenManager;
        this.fieldScreenFactory = fieldScreenFactory;
    }
    /**
     * Called when the plugin has been enabled.
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // register ourselves with the EventPublisher
        eventPublisher.register(this);
    }

    /**
     * Called when the plugin is being disabled or removed.
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        // unregister ourselves with the EventPublisher
        eventPublisher.unregister(this);
    }
    @EventListener
    public void onIssueEvent(PluginEnabledEvent pluginEnabledEvent){
        CustomField cfMergeIssue=null;
        if(PLUGIN_KEY.equals(pluginEnabledEvent.getPlugin().getKey())){
            Collection<CustomField> customFields = customFieldManager.getCustomFieldObjectsByName(MergeIssuesFunction.MERGE_INTO_ISSUE);
            if (null == customFields || customFields.isEmpty()) {
                try {
                    cfMergeIssue = customFieldManager.createCustomField(MergeIssuesFunction.MERGE_INTO_ISSUE, "Issue picker for target issue in merge.",
                            customFieldManager.getCustomFieldType(PLUGIN_KEY + ":merge-custom-field"), NO_CUSTOMFIELD_SEARCHER, allProjects, allIssues);
                } catch (GenericEntityException e) {
                    e.printStackTrace();
                }
            } else {
                cfMergeIssue=customFields.iterator().next();
            }
            boolean createMergeScreen= true;
            for(FieldScreen fieldScreen: fieldScreenManager.getFieldScreens()){
                if(MERGE_SCREEN.equals(fieldScreen.getName())){
                    createMergeScreen = false;
                    break;
                }
            }
            if(createMergeScreen && null!=cfMergeIssue){
                FieldScreen mergeScreen = fieldScreenFactory.createScreen();
                mergeScreen.setName(MERGE_SCREEN);
                mergeScreen.setDescription("Transition screen for merge.");
                fieldScreenManager.createFieldScreen(mergeScreen);
              mergeScreen.addTab("Field Tab");
              mergeScreen.getTab(0).addFieldScreenLayoutItem(cfMergeIssue.getId());
            }
        }
    }
}
